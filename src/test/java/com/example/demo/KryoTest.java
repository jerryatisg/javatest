package com.example.demo;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.example.demo.graphql.Account;
import com.example.demo.graphql.Holding;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KryoTest {

    private List<Account> getTestData(int size){
        List<Account> accountList = new ArrayList<>();
        for(int i=0;i<size;i++){
            accountList.add(getOneAccount());
        }
        return accountList;
    }

    private Account getOneAccount(){
        return new Account(
                RandomStringUtils.randomAlphanumeric(6),
                RandomStringUtils.randomAlphanumeric(30),
                Arrays.asList(
                        new Holding(RandomStringUtils.randomAlphanumeric(8),
                                RandomStringUtils.randomAlphanumeric(30),
                                new Random().nextInt(50) + 1),
                        new Holding(RandomStringUtils.randomAlphanumeric(8),
                                RandomStringUtils.randomAlphanumeric(30),
                                new Random().nextInt(100) + 1))
                );
    }



    private List<Account> testJson(List<Account> testdata) throws Exception{
        FileWriter writer
                = new FileWriter("json.txt");

        long time_1 = System.currentTimeMillis();
        ObjectMapper mapper = new ObjectMapper();
        //System.out.println("Time before write: " + time_1);

        writer.write(mapper.writeValueAsString(testdata));
        writer.flush();
        writer.close();
        long time_4 = System.currentTimeMillis();
        //System.out.println("Time after write: " + time_4);


        long time_5 = System.currentTimeMillis();
        //System.out.println("Time before read: " + time_5);

        String value= Files.readString(Path.of("json.txt"));
        List<Account> inputaccounts = (List<Account>)mapper.readValue(value, ArrayList.class);

        long time_6 = System.currentTimeMillis();
        //System.out.println("Time after read: " + time_6);


        System.out.println("ObjectMapper: Time took to write " + testdata.size() + " obj: " + (time_4-time_1) );
        System.out.println("ObjectMapper: Time took to read " + testdata.size() + " obj: " + (time_6-time_5) );
        return inputaccounts;
    }

    private List<Account> serialization(List<Account> testdata) throws Exception{
        FileOutputStream fileOutputStream
                = new FileOutputStream("serialization.txt");
        ObjectOutputStream objectOutputStream
                = new ObjectOutputStream(fileOutputStream);

        long time_1 = System.currentTimeMillis();
        //System.out.println("Time before write: " + time_1);

        objectOutputStream.writeObject(testdata);
        objectOutputStream.flush();
        objectOutputStream.close();
        long time_4 = System.currentTimeMillis();
        //System.out.println("Time after write: " + time_4);


        FileInputStream fileInputStream
                = new FileInputStream("serialization.txt");
        ObjectInputStream objectInputStream
                = new ObjectInputStream(fileInputStream);
        long time_5 = System.currentTimeMillis();
        //System.out.println("Time before read: " + time_5);

        List<Account> inputaccounts = (List<Account>)objectInputStream.readObject();
        objectInputStream.close();
        long time_6 = System.currentTimeMillis();
        //System.out.println("Time after read: " + time_6);


        System.out.println("Serializable: Time took to write " + testdata.size() + " obj: " + (time_4-time_1) );
        System.out.println("Serializable: Time took to read " + testdata.size() + " obj: " + (time_6-time_5) );
        return inputaccounts;
    }

    private List<Account> testKryo(List<Account> testdata) throws Exception{
        Kryo kryo = new Kryo();
        kryo.setRegistrationRequired(false);
        Output output = new Output(new FileOutputStream("file.dat"));
        Input input = new Input(new FileInputStream("file.dat"));


        long time_1 = System.currentTimeMillis();
//        System.out.println("Time before write: " + time_1);

        kryo.writeObject(output, testdata);
        output.close();
        long time_4 = System.currentTimeMillis();
//        System.out.println("Time after write: " + time_4);

        List<Account> readData= kryo.readObject(input, ArrayList.class);
        input.close();
        long time_5 = System.currentTimeMillis();
//        System.out.println("Time after read: " + time_5);


        System.out.println("Kryo: Time took to write " + testdata.size() + " obj: " + (time_4-time_1) );
        System.out.println("Kryo: Time took to read " + testdata.size() + " obj: " + (time_5-time_4) );

        return readData;
    }


    @Test
    public void testSerialization() throws Exception{


            int sizeToTest = 100000;
            List<Account> testdata = getTestData(sizeToTest);

            var readData = testKryo(testdata);

            testJson(testdata);


            serialization(testdata);


            assertNotNull(readData);

    }

    /*** result in ms
    Kryo: Time took to write 100000 obj: 185
    Kryo: Time took to read 100000 obj: 129
    ObjectMapper: Time took to write 100000 obj: 423
    ObjectMapper: Time took to read 100000 obj: 398
    Serializable: Time took to write 100000 obj: 3004
    Serializable: Time took to read 100000 obj: 4129
    ***/


}
