package com.example.demo.ws;

import lombok.Data;

@Data
public class Message {
    private String from;
    private String text;
}
