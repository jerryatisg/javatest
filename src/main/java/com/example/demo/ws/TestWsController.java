package com.example.demo.ws;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@CrossOrigin(origins ="*")
public class TestWsController {

    @MessageMapping("/chat")
    @SendTo("/topic/messages")
    public OutputMessage processGroupMsg(Message message) throws Exception {
        //Thread.sleep(1000); // simulated delay
        System.out.println(message.toString());
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        return new OutputMessage("Server received group msg:" +message.toString(), time);
    }


    @MessageMapping("/privatechannel")
    @SendToUser(destinations="/queue/privatemsg", broadcast=false)
    public OutputMessage processPrivateMsg(Message message) throws Exception {
        //Thread.sleep(1000); // simulated delay
        System.out.println(message.toString());
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        return new OutputMessage("Server received private message:" +message.toString(), time);
    }
}


