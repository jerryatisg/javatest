package com.example.demo.ws;

import lombok.Data;

@Data
public class OutputMessage {
    private final String msg;
    private final String time;

    public OutputMessage(final String msg,  final String time) {
        this.msg = msg;
        this.time = time;
    }
}
