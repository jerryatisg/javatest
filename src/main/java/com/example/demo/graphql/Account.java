package com.example.demo.graphql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {
    private String accountId;
    private String accountName;

    private List<Holding> holdings;

    public static List<Account> getSampleAccounts(){
        Holding h1 = new Holding("AAPL","apple",1);
        Holding h2 = new Holding("MSFT", "microsoft",2);
        Holding h3 = new Holding("TSLA", "tesla" ,3);
        Holding h4 = new Holding("GOOG", "GOOGLE", 4);

        Account acct1 = new Account("ACCT1", "Account a", Arrays.asList(h1,h2));

        Account acct2 = new Account("ACCT2", "Account b", Arrays.asList(h3,h2, h4));
        return Arrays.asList(acct1, acct2);
    }
}
