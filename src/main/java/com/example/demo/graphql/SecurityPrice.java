package com.example.demo.graphql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurityPrice {
    private String securityId;
    private double price;
    private LocalDateTime timestamp;
}
