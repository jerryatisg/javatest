package com.example.demo.graphql;

import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

// https://www.viralpatel.net/spring-boot-graphql-subscription-realtime-api/

/*
subscription {
  securityPrice(securityId: "AAPL") {
    securityId
    price
    timestamp
  }
}
*/

@Component
public class SubscriptionResolver  implements GraphQLSubscriptionResolver {

    public Publisher<SecurityPrice> securityPrice(String securityId) {
        Random random = new Random();
        return Flux.interval(Duration.ofSeconds(2))
                .map(num -> new SecurityPrice(securityId, random.nextDouble(), LocalDateTime.now()));
    }
}
