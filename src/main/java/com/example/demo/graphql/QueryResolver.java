package com.example.demo.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

// http://localhost:32000/graphiql
// https://www.baeldung.com/spring-graphql

/*
{
  getAccountById(id: "ACCT2") {
    accountName
    holdings {
      securityId
      share
    }
  }
}

{
  accounts {
    accountId
    accountName
    holdings {
      securityId
      share
      securityName
    }
  }
}

*/

@Component
public class QueryResolver implements GraphQLQueryResolver {
    @Autowired
    private AccountService accountService;

    public Account getAccountById(String id) {
        return accountService.getAccountById(id);
    }

    public List<Account> accounts(){
        return accountService.getAllAccounts();
    }
}
