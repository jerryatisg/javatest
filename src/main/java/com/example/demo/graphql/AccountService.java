package com.example.demo.graphql;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {
    private List<Account> accounts;

    public AccountService(){
        accounts = Account.getSampleAccounts();
    }

    public Account getAccountById(String id){
        return accounts.stream().filter(a -> a.getAccountId().equalsIgnoreCase(id)).findFirst().orElse(null);
    }

    public List<Account> getAllAccounts(){
        return Account.getSampleAccounts();
    }

}
