package com.example.demo.graphql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Holding implements Serializable {
    private String securityId;
    private String securityName;
    private int share;
}
