package com.example.demo.sse;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.*;
import java.util.stream.Stream;

@Service
public class SseMessageService {

    public String getRandomUser() {
        String[] users = "adam,tom,john,mike,bill,tony".split(",");
        return users[new Random().nextInt(users.length)];
    }

    public Flux<SseMessage> getMessages() {
        Flux<Long> interval = Flux.interval(Duration.ofSeconds(1));

        Flux<SseMessage> messageFlux = Flux.fromStream(Stream.generate(() -> new SseMessage(getRandomUser(), new Date())));
        return Flux.zip(interval, messageFlux).map(Tuple2::getT2);
    }
}


