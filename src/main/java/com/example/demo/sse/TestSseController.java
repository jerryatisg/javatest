package com.example.demo.sse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@CrossOrigin(origins ="*")
@RequestMapping("/testsse")
public class TestSseController {
    @Autowired
    SseMessageService sseMessageService;

    @GetMapping(value = {""}, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<SseMessage> sseEvents(){
        return sseMessageService.getMessages();
    }

}
