package com.example.demo.sse;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor()
public class SseMessage {
    String user;
    Date when;

}
