package com.example.demo.resilience;


import io.github.resilience4j.bulkhead.*;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.retry.Retry;
import io.vavr.CheckedFunction0;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

//  https://resilience4j.readme.io/docs

@RestController
@CrossOrigin(origins ="*")
@RequestMapping("/testres")
public class TestResController {

    private static int cbct = 0 ;
    private static int bhct = 0;

    @Autowired
    TestDataSupplier dataSupplier;

    private final CircuitBreaker circuitBreaker;
    private Bulkhead bulkhead;

    public TestResController(){
        CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
                .failureRateThreshold(50)
                .waitDurationInOpenState(Duration.ofSeconds(5))
                .permittedNumberOfCallsInHalfOpenState(2)
                .minimumNumberOfCalls(3)
                .slidingWindowSize(5)
                .recordExceptions(Exception.class).build();

        circuitBreaker = CircuitBreaker.of("testName", circuitBreakerConfig);



        BulkheadConfig config = BulkheadConfig.custom()
                .maxConcurrentCalls(1)
//                .maxWaitDuration(Duration.ofSeconds(5))
                .build();
        bulkhead = Bulkhead.of("testbh", config);
        BulkheadRegistry registry = BulkheadRegistry.of(config);
        bulkhead = registry.bulkhead("testbh");

    }


    /**
     * Calling /retry will show the behavior of the retry capability
     */
    @GetMapping(value = {"/retry"})
    public String testrt(){
        return testretry();
    }

    private String testretry(){
        Retry retry = Retry.ofDefaults("backendName");

        CheckedFunction0<String> retryableSupplier = Retry
                .decorateCheckedSupplier(retry, dataSupplier::getRtData);

        Try<String> result = Try.of(retryableSupplier)
                .recover((throwable) -> "NO DATA");

        return result.get();
    }

    /**
     * calling /cb/success will always get good data,
     * calling /cb/anythingelse will trigger circuit breaker after 3 calls
     *    - notice the wait time to enter half_open state and calles allowe in the half_open state
     */
    @GetMapping(value = {"/cb/{input}"})
    public String testcb(@PathVariable(value = "input") String input){
        cbct ++;

        CheckedFunction0<String> decoratedSupplier = CircuitBreaker
                .decorateCheckedSupplier(circuitBreaker, () -> dataSupplier.getCbData(input, cbct));

        Try<String> result = Try.of(decoratedSupplier).recover((throwable) -> "NO DATA");

        return result.get();
    }

    /**
     *  run ab -n 20 -c 10 http://localhost:32000/testres/bh
     *  to see the result
     */
    @GetMapping(value = {"/bh"})
    public String testbh(){
        bhct++;

        System.out.println(bhct);
        CheckedFunction0<String> decoratedBhSupplier = Bulkhead
                .decorateCheckedSupplier(bulkhead, dataSupplier::getBhData);
        Try<String> result = Try
                .of(decoratedBhSupplier)
                .recover(
                        (throwable) ->{
                            System.out.println("bad data");
                            return "bad data";
                        }
                    );

        return result.get();
    }

}


