package com.example.demo.resilience;

import org.springframework.stereotype.Component;

@Component
public class TestDataSupplier {
    private static int ct =0;
    private static int cbct = 0;

    public String getRtData() throws Exception{
        ct ++;
        System.out.println(ct);
        if(ct % 4 == 0) {
            System.out.println("correct data");
            return "correct data";
        }
        System.out.println("should throw exception");
        throw new Exception("exception");
    }

    public String getCbData(String input, int parentCt) throws Exception{
        cbct ++;
        System.out.println(input);
        System.out.printf("parent ct: %d local ct: %d%n", parentCt, cbct);
        if(input.equalsIgnoreCase("success")) {
            System.out.println("correct cb data");
            return "correct cb data";
        }
        Thread.sleep(2000);
        System.out.println("cb should throw exception");
        throw new Exception("cb exception");
    }

    public String getBhData() throws  Exception{
        Thread.sleep(2000);
        System.out.println("good data");
        return "good data";
    }

}
