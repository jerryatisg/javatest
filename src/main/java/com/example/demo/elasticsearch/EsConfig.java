package com.example.demo.elasticsearch;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/*
To start elasticsearch, from elasticsearch folder
 bin/elasticsearch
*/

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.example.demo.elasticsearch")
@ComponentScan(basePackages = { "com.example.demo.elasticsearch" })
public class EsConfig {

    @Bean
    RestHighLevelClient client() {
        ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo("localhost:9200")
                .build();

        return RestClients.create(clientConfiguration)
                .rest();
    }

    @Bean
    public /*ElasticsearchOperations*/ ElasticsearchRestTemplate elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(this.client());
    }
}