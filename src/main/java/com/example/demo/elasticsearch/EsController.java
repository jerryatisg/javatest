package com.example.demo.elasticsearch;

import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.Operator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Arrays.asList;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@RestController
@RequestMapping("/es")
public class EsController {
    @Autowired
    private ElasticsearchRestTemplate elasticsearchTemplate;

    @Autowired
    private RestHighLevelClient client;

    private ArticleRepository articleRepository;

    @Autowired
    public EsController(ArticleRepository articleRepository){
        this.articleRepository= articleRepository;
    }

    @GetMapping("testset")
    public String setup(){
        this.before();
        return "done";
    }

    @GetMapping("getdata")
    public long getdata(){
        final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().
                withQuery(matchQuery("title", "Search engines").operator(Operator.AND))
                .build();
        final SearchHits<Article> articles = elasticsearchTemplate.search(searchQuery, Article.class, IndexCoordinates.of("blog"));

        return articles.getTotalHits();
    }

    public void before() {
        final Author johnSmith = new Author("John Smith");
        final Author johnDoe = new Author("John Doe");
        Article article = new Article("Spring Data Elasticsearch");
        article.setAuthors(asList(johnSmith, johnDoe));
        article.setTags("elasticsearch", "spring data");
        try {
            articleRepository.save(article);
        }catch(Exception e){
            System.out.println(e.getMessage()); //looks like the response is differnet, maybe the version mismatch
        }

        article = new Article("Search engines");
        article.setAuthors(asList(johnDoe));
        article.setTags("search engines", "tutorial");
        try {
            articleRepository.save(article);
        }catch(Exception es){}

        article = new Article("Second Article About Elasticsearch");
        article.setAuthors(asList(johnSmith));
        article.setTags("elasticsearch", "spring data");
        try {
            articleRepository.save(article);
        }catch(Exception es){}

        article = new Article("Elasticsearch Tutorial");
        article.setAuthors(asList(johnDoe));
        article.setTags("elasticsearch");
        try {
            articleRepository.save(article);
        }catch(Exception es){}
    }

}
