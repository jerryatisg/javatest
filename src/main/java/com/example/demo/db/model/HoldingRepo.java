package com.example.demo.db.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HoldingRepo extends JpaRepository<Holding, Integer> {
}
