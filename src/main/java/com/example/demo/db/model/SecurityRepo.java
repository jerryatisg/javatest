package com.example.demo.db.model;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Table;

public interface SecurityRepo extends JpaRepository<Security, Integer> {
}
