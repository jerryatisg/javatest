package com.example.demo.db.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountRepo extends JpaRepository<Account, Integer> {

    @Query(value="SELECT * FROM account where id=1", nativeQuery=true)
    List<Account> getAccountTest();
}
