package com.example.demo.db.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Field;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Holding {

    @Id
    private int id;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "security_id")
    private Security security;

    @JsonManagedReference
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "account_id")
    private Account account;

}
