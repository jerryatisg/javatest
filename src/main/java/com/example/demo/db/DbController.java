package com.example.demo.db;

import com.example.demo.db.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/db")
public class DbController {
    private AccountRepo accountRepo;
    private SecurityRepo securityRepo;
    private HoldingRepo holdingRepo;

    @Autowired
    public DbController(
            AccountRepo accountRepo,
            SecurityRepo securityRepo,
            HoldingRepo holdingRepo){
        this.accountRepo = accountRepo;
        this.securityRepo = securityRepo;
        this.holdingRepo = holdingRepo;
    }

    @GetMapping("accounts")
    public ResponseEntity<List<Account>> getAccounts(){
        return new ResponseEntity<>(accountRepo.findAll(), HttpStatus.OK);
    }

    @GetMapping("accounts/test")
    public ResponseEntity<List<Account>> getAccountsTest(){
        return new ResponseEntity<>(accountRepo.getAccountTest(), HttpStatus.OK);
    }

    @GetMapping("securities")
    public ResponseEntity<List<Security>> getSecurities(){
        return new ResponseEntity<>(securityRepo.findAll(), HttpStatus.OK);
    }

    @GetMapping("holdings")
    public ResponseEntity<List<Holding>> getHoldings(){
        return new ResponseEntity<>(holdingRepo.findAll(), HttpStatus.OK);
    }


    @PostMapping("accounts")
    public ResponseEntity<String> insertAccount(Account account){
        return new ResponseEntity<>("inserted", HttpStatus.CREATED);
    }

}
