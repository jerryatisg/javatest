## Java currency

### Thread
```java
Thread t = new Thread(()->System.out.println("hello"))
t.start()
```

### Executors/Future
```java
var e = Executors.newSingleThreadExecutor()
var f = e.submit(() -> "hello")
f.get()
```

### ForkJoinPool
```java
var e = ForkJoinPool.commonPool()
var f = e.submit(() -> "hello")
f.get()
```

### CompleteableFuture
```java
var cf = new CompletableFuture<String>()
cf.complete("hello")
cf.get()

var cf = new CompletableFuture<String>()
cf.completeExceptionally(new ArithmeticException("error"))
cf.get()
```

### CompleteableFuture compose
```
CompletableFuture.supplyAsync(() -> "hello").thenApplyAsync(x->x + " aha").thenAcceptAsync(System.out::println)
CompletableFuture.supplyAsync(() -> "hello").thenCombineAsync(CompletableFuture.supplyAsync(()->" aha"), (x, y)->x+y).thenAcceptAsync(System.out::println)
CompletableFuture.supplyAsync(() -> "hello").thenCombineAsync(CompletableFuture.supplyAsync(()->" aha"), (x, y)->x+y).exceptionally(t->"encountererror").thenAcceptAsync(System.out::println)
CompletableFuture.failedFuture(new IllegalStateException("error")).thenCombineAsync(CompletableFuture.supplyAsync(()->" aha"), (x, y)->x+y).exceptionally(t->"encounter error").thenAcceptAsync(System.out::println)


var cf = new CompletableFuture<String>()
cf.completeOnTimeout("timeout", 4, TimeUnit.SECONDS)
cf.get()
```

