## Java 

## Testing of serialization
- Kryo
- ObjectMapper
- Serializable
- [Source](src/test/java/com.example.demo/KryoTest.java)
- Result in millisecond (testing write to and read from local file)
  - in short, Kryo is a few times faster than ObjectMapper, which in turn is several to 10 times faster than default Serializable
```
    Kryo: Time took to write 100000 obj: 185
    Kryo: Time took to read 100000 obj: 129
    ObjectMapper: Time took to write 100000 obj: 423
    ObjectMapper: Time took to read 100000 obj: 398
    Serializable: Time took to write 100000 obj: 3004
    Serializable: Time took to read 100000 obj: 4129

```
- Several good references
  - a github project: https://github.com/eishay/jvm-serializers/wiki
  - https://www.alibabacloud.com/blog/an-introduction-and-comparison-of-several-common-java-serialization-frameworks_597900
  - https://blog.qaware.de/posts/binary-data-format-comparison/
  - https://blog.softwaremill.com/the-best-serialization-strategy-for-event-sourcing-9321c299632b
- __Maybe should try proto buffer__



## Graph QL with Springboot
- GraphiQL UI: http://localhost:32000/graphiql
- Sample queries 
  - from Graphiql UI, 
  - or can do http POST to http://localhost:32000/graphql
```graphql

query {
  getAccountById(id: "ACCT2") {
    accountName
    holdings {
      securityId
      share
    }
  }
}

query {
  accounts {
    accountId
    accountName
    holdings {
      securityId
      share
      securityName
    }
  }
}
```
- Sample subscription
  - from Graphiql UI, through ws://localhost:32000/subscriptions  
```
subscription {
  securityPrice(securityId: "AAPL") {
    securityId
    price
    timestamp
  }
}
```


## Resilience4j library
- Resilence4j document - https://resilience4j.readme.io/docs
- Some interesting explanation of the patterns
  - https://docs.microsoft.com/en-us/azure/architecture/patterns/bulkhead
  - https://docs.microsoft.com/en-us/azure/architecture/patterns/circuit-breaker
- Retry
```
  - http://localhost:32000/testres/retry
```
  - The console will print out the retries
- Circuitbreaker
```
  - http://localhost:32000/testres/cb/success
  - http://localhost:32000/testres/cb/fail
```
  - Try the fail url many time, console will print out the after some calles, circuit breaker will return right away without calling backend service. The it will try to call again 
- Bulkhead
  - try comand
```
ab -n 20 -c 10 http://localhost:32000/testres/bh
```
  - result will show that one 1 out of 10 get the correct backend result, that is due to the limitation of the bulkhead configuration

## Server side event with Springboot
- http://localhost:32000/testsse
- [source](src/main/java/com.example.demo/sse/TestSseController.java)


## Websocket
- Simple websocket app 
  - WebSocketConfig
```
# command:
wscat -c ws://localhost:32000/user

# send message
{"user":"anyusername"}

```
- Websocket broker
  - WebsocketBrokerConfig
  - This one need more complicated client
  - Took this example: https://www.baeldung.com/websockets-spring
  
## Elastic search
- To start elasticsearch, from elasticsearch folder
```  
  bin/elasticsearch
```
  
## Kafka

- Start server:
```
bin/zookeeper-server-start.sh config/zookeeper.properties

bin/kafka-server-start.sh config/server.properties
```

-Create topic:
```
bin/kafka-topics.sh --create --topic example  --bootstrap-server localhost:9092
```

-To publish message
```
curl -X POST -F 'message=test' http://localhost:32000/kafka/publish
```

Consumer will receive message and print out
  